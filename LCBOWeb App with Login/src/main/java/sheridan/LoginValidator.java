package sheridan;

public class LoginValidator {
	
	private LoginValidator() {
		
	}
	
	public static boolean isValidLoginName( String loginName ) {
		if(loginName.contains(" ")) {
			return false;
		}
		
		if(!loginName.matches("[A-Za-z0-9]+")) {
			return false;
		}

		if(loginName.length() > 100) {
			return false;
		}
		return loginName.length( ) > 4;
	}
}
