package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue(LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginException() {
		assertFalse(LoginValidator.isValidLoginName("ab"));
	}
	
	@Test
	public void testIsValidLoginBoundryIn() {
		assertTrue(LoginValidator.isValidLoginName("abcde"));
	}

	@Test
	public void testIsValidLoginBoundryOut() {
		assertFalse(LoginValidator.isValidLoginName("abcd"));
	}

	@Test
	public void testIsValidLoginExceptionSpaces() {
		assertFalse(LoginValidator.isValidLoginName("a bcdefg"));
	}
	
	@Test
	public void testIsValidLoginExceptionSpecialCharacters() {
		assertFalse(LoginValidator.isValidLoginName("AB$CDE"));
	}
	
	@Test
	public void testIsValidLoginExceptionEmpty() {
		assertFalse(LoginValidator.isValidLoginName(""));
	}
	
	@Test
	public void testIsValidLoginExceptionToLong() {
		assertFalse("test",LoginValidator.isValidLoginName("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123"));
	}
	
	
}
